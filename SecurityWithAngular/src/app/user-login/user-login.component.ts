import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginuserService } from '../loginuser.service';
import { User } from '../user';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  user:User = new User;

  constructor(private userService:LoginuserService, private route:Router) { }

  ngOnInit(): void {
  }


  userLogin(){
    console.log(this.user);
    this.userService.loginUser(this.user).subscribe(
      data =>{
      alert("login successful");
      this.route.navigate(["login"]);
    },(error: any)=>alert("Please enter valid userid and password"));
  }

}
