import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginuserService } from '../loginuser.service';
import { User } from '../user';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
   user = new User();

  constructor(private _service:LoginuserService, private _route:Router) { }

  ngOnInit(): void {
  }

  addUser(){
    this._service.addUser(this.user).subscribe(data=>{
      alert("User registered successfully!!");
      //this._route.navigate(["userlogin"]);
    },error=>alert("Failed Registration "))

  }



}
