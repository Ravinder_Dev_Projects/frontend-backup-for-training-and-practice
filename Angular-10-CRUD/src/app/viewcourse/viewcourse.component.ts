import { Component, OnInit } from '@angular/core';
import {NgserviceService} from '../ngservice.service';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import { Course } from '../course';
@Component({
  selector: 'app-viewcourse',
  templateUrl: './viewcourse.component.html',
  styleUrls: ['./viewcourse.component.css']
})
export class ViewcourseComponent implements OnInit {
  courses = new Course();

  constructor(private _route:Router, private _service: NgserviceService, private _activatedRouter: ActivatedRoute) { }
  ngOnInit(): void {

    let id = this._activatedRouter.snapshot.paramMap.get('id');

    let id1 = this._activatedRouter.snapshot.params['get'](id);
var b = +id1;
    // var a:any =+id ;
   this._service.FetchCourseByIdFromRemote(b).subscribe(
    data=>{
      console.log("data received");
      this.courses = data;
    },
      error => console.log("Exception Occured")
    )
  }
  gotolist() {
    this._route.navigate(['courselist']);
  }
}
