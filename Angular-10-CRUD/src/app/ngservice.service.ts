import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from './course';

@Injectable({
  providedIn: 'root'
})
export class NgserviceService {

  constructor(private _http:HttpClient) { }

  fetchCourseListFromRemote(): Observable<any>{
    return this._http.get<any>('http://localhost:5000/course');
  }
  addCourseListFromRemote(course:Course): Observable<any>{
    return this._http.post<any>('http://localhost:5000/course',course);
  }
  updatedCourseListFromRemote(course:Course): Observable<any>{
    return this._http.post<any>('http://localhost:5000/course',course);
  }
  deleteCourseByIdFromRemote(id:number): Observable<any>{
    return this._http.delete<any>('http://localhost:5000/course' + id);
  }
  FetchCourseByIdFromRemote(id:number): Observable<any>{
    return this._http.get<any>('http://localhost:5000/course'+id);
  }
}
